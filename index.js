const path = require('path');
const express = require('express');
const mongoose = require('mongoose');
const expressHbs = require('express-handlebars');
const cookieParser = require('cookie-parser');
require('dotenv').config({path: path.join(__dirname, 'config/config.env')});

const authRouter = require('./routes/authRoutes');
const { requireAuth, checkUser } = require('./middleware/authMiddleware');

const app = express();

// Создание движка представлений
const hbs = expressHbs.create({
    defaultLayout: 'layout',
    extname: 'hbs',
  });

// Добавления движка представлений
app.engine('hbs', hbs.engine);
 
// Установка движка представлений
app.set('view engine', 'hbs');

// Статические файлы
app.use(express.static(path.join(__dirname, '/public')));

// json parser
app.use(express.json());

// cookie parser
app.use(cookieParser());


// routes
app.get('*', checkUser);

app.get('/', (req, res) => {
    res.render('home')
})

app.get('/content', requireAuth, (req, res) => {
    res.render('content')
})

app.use(authRouter);



const MONGO_URI = process.env.MONGO_URI;
const port = process.env.PORT || 3001;


mongoose.connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
})
.then(result => {
    app.listen(port, console.log("server running"))
})
.catch(err => {
    console.log(err)
})
